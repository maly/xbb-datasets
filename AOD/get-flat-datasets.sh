#!/usr/bin/env bash

for DSID in 426351 426347 426345; do
    rucio ls --short mc16_13TeV:mc16_13TeV.$DSID*.AOD.*
done
